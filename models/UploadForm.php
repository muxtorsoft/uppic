<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 15.05.2019
 * Time: 10:07
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, gif, jpg, jpeg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $title = $this->imageFile->baseName;
            $filename = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if(Pictures::find()->where(['filename'=>$filename])->exists()) die('Такое рисунок уже имеется в базу');
            $ext = $this->imageFile->extension;
            $filesize = $this->imageFile->size;
            $this->imageFile->saveAs('uploads/original/' . $filename,false);
            $this->imageFile->saveAs('uploads/watermark/' . $filename);
            // Добавить водяный знак
            $toWater = $_SERVER['DOCUMENT_ROOT']. '/uploads/watermark/' . $filename;
            $this->textwatermark($ext, $toWater, 'Watermark from Ulugbek!', $toWater);

            $model = new Pictures();
            $model->title = $title;
            $model->filename = $filename;
            $model->saved_filename = $filename;
            $model->extension = $ext;
            $model->path = 'uploads';
            $model->filesize = $filesize;
            $model->save(false);
            return $filename;
        } else {
            return false;
        }
    }

    // Функция добавления текст к рисунок
    public function textwatermark($ext='jpeg', $src, $watermark, $save=NULL) {
        list($width, $height) = getimagesize($src);
        $image_p = imagecreatetruecolor($width, $height);
        if($ext=='png'){
            $image = imagecreatefrompng($src);
        }elseif ($ext=='gif'){
            $image = imagecreatefromgif($src);
        }else{
            $image = imagecreatefromjpeg($src);
        }

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        $txtcolor = imagecolorallocate($image_p, 255, 255, 255);
        $font = __DIR__.'/monofont.ttf';
        $font_size = 50;
        imagettftext($image_p, $font_size, 0, ($height/2-25), $height/2+25, $txtcolor, $font, $watermark);
        if ($save<>'') {
            if($ext=='png'){
                imagepng ($image_p, $save, 9);
            }elseif ($ext=='gif'){
                imagegif ($image_p, $save);
            }else{
                $image = imagejpeg ($image_p, $save, 100);
                imagedestroy($image);
            }
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($image_p, null, 100);
        }
        imagedestroy($image_p);
    }

    // Функция добавления watermark к рисунок
    public function watermarkImage($ext='jpeg', $SourceFile, $WaterMark, $DestinationFile=NULL, $opacity) {

        $main_img = $SourceFile;
        $watermark_img = $WaterMark;
        $padding = 3;
        $opacity = $opacity;

        $watermark = imagecreatefromgif($watermark_img); // создать водяный знак
        $image = imagecreatefromjpeg($main_img); // создать основную рисунок

        if(!$image || !$watermark) die("Ошибка: не можем загрузить основная рисунок или водяный знак!");

        $watermark_size = getimagesize($watermark_img);
        $watermark_width = $watermark_size[0];
        $watermark_height = $watermark_size[1];

        $image_size = getimagesize($main_img);
        $dest_x = $image_size[0] - $watermark_width - $padding;
        $dest_y = $image_size[1] - $watermark_height - $padding;

        // Копировать водяный знак на осноную рисунок
        imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $opacity);
        if ($DestinationFile<>'') {
            imagejpeg($image, $DestinationFile, 100);
        }
        else {
            header('Content-Type: image/jpeg');
            imagejpeg($image);
        }
        imagedestroy($image);
        imagedestroy($watermark);
    }
}