<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%pictures}}".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $filename
 * @property string $saved_filename
 * @property string $extension
 * @property double $filesize
 * @property string $path
 * @property int $is_read
 * @property int $created_at
 * @property int $updated_at
 */
class Pictures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pictures}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['filename', 'saved_filename'], 'required'],
            [['filesize'], 'number'],
            [['is_read', 'created_at', 'updated_at'], 'integer'],
            [['title', 'filename', 'saved_filename'], 'string', 'max' => 255],
            [['extension'], 'string', 'max' => 10],
            [['path'], 'string', 'max' => 2048],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'filename' => 'Filename',
            'saved_filename' => 'Saved Filename',
            'extension' => 'Extension',
            'filesize' => 'Filesize',
            'path' => 'Path',
            'is_read' => 'Is Read',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
