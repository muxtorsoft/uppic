<?php

namespace app\controllers;

use app\models\Pictures;
use Yii;
use app\models\UploadForm;
use yii\web\UploadedFile;

class UploadController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $filename = $model->upload();
            if ($filename!=false) {
                // file is uploaded successfully
                return $this->redirect(['/upload/uploaded','filename'=>$filename]);
            }
        }

        return $this->render('index', ['model' => $model]);
    }

    public function actionUploaded($filename)
    {
        $this->layout = 'blank';
        /** @var $model Pictures*/
        $model = Pictures::find()->where(['filename'=>$filename])->one();
        if($model==null) return 'Нет рисунок!';
        if($model->is_read==null){
            $path = $model->path.'/original/'.$model->saved_filename;
            $model->is_read = 1;
            $model->update(false);
        }else{
            $path = $model->path.'/watermark/'.$model->saved_filename;
        }
        return $this->render('image', ['model' => $model, 'src'=>'/'.$path]);
    }

}
