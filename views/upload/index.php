<?php
/* @var $this yii\web\View */
?>
<?php
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*'])->label('Рисунок') ?>

<button>Загрузить</button>

<?php ActiveForm::end() ?>