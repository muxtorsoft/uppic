<?php

use yii\db\Migration;

/**
 * Class m190515_045725_picture
 */
class m190515_045725_picture extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE {{%pictures}} (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT NULL,
	`filename` VARCHAR(255) NOT NULL,
	`saved_filename` VARCHAR(255) NOT NULL,
	`extension` VARCHAR(10) NULL DEFAULT NULL,
	`filesize` FLOAT NULL DEFAULT NULL,
	`path` VARCHAR(2048) NULL DEFAULT NULL,
	`is_read` INT(11) NULL DEFAULT NULL,
	`created_at` INT(11) NULL DEFAULT NULL,
	`updated_at` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190515_045725_picture cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190515_045725_picture cannot be reverted.\n";

        return false;
    }
    */
}
